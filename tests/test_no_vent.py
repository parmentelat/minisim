from minisim import Zone, Weather, Inhabitant
import pandas as pd
import pytest


@pytest.fixture
def testenv():
    w = Weather(400)
    z = Zone(50, 0.42, w)
    names = ['David', 'Christophe', 'Antoine', 'Guillaume', 'Kamel', 'Adelaide', 'Nassiba', 'Sergei', 'Thierry']
    i = [Inhabitant(name, 10) for name in names]
    date_range = pd.date_range(start='2017-03-22 13:30:00',
                               end='2017-03-22 17:30:00',
                               freq='1T')
    test_env = dict()
    test_env['w'] = w
    test_env['z'] = z
    test_env['i'] = i
    test_env['date_range'] = date_range
    return test_env


def test_no_vent(testenv):
    """
    Test if the CO2 concentration increases without ventilation or infiltration
    """
    testenv['z'].add_inhabitants(testenv['i'])
    ts = testenv['z'].simulate(testenv['date_range'])
    l = [e for e in ts.diff() if e < 0]
    assert l is False

