from minisim import Zone, Inhabitant, Infiltration, Weather

volume = 100
infiltration = Infiltration('infiltration', 30)
weather = Weather(20)

def test1():
    zone = Zone(volume, infiltration, weather)
    in1 = Inhabitant('in1', 12)
    in2 = Inhabitant('in2', 24)
    zone.add_inhabitants(in1, in2)
    assert len(zone.inhabitants) == 2
    
def test2():
    zone = Zone(volume, infiltration, weather)
    in1 = Inhabitant('in1', 12)
    in2 = Inhabitant('in2', 24)
    zone.add_inhabitants(in1, in1)
    assert len(zone.inhabitants) == 1
    
