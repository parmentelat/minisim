from __future__ import print_function, division
from minisim import Zone, Weather, Infiltration, Inhabitant, Ventilation
import pandas as pd
import numpy as np

import pytest

@pytest.mark.skip(reason="no way of currently testing this")
def test_true_sim():
    """
    test par rapport à une simulation parfaite
    """
    # sim parameters
    initial_CO2 = 400  #ppm
    volume = 125 #m3
    ven_flow_rate = volume*0.5 # m3/h
    inf_flow_rate = volume*0.5 # m3/h
    occ_flow_rate = 1e-4*3600 # m3/h
    outdoor_CO2 = 400. #ppm
    concentration = 36000. #ppm
    timestep = 600./3600. #h
    n_occ=1
    # True sim
    a = 1. - (ven_flow_rate + inf_flow_rate + occ_flow_rate) * timestep/ volume
    b = (ven_flow_rate + inf_flow_rate) * timestep / volume * outdoor_CO2 \
        + occ_flow_rate* n_occ * timestep / volume * concentration
    r = b / (1. - a)
    df_exact = pd.Series(index=pd.date_range(start='01-january-2017',
                                             periods=1 * 24 * 6,
                                             freq='10MIN'))

    CO2_exact = []

    idx = 0
    for i in df_exact.index:
        CO2_exact.append(pow(a, idx) * (initial_CO2 - r) + r)
        idx += 1
    df_exact[:] = np.array(CO2_exact)

    # minisim sim
    occs = []
    for i in range(0,n_occ):
        occs.append(Inhabitant('occ_{}'.format(i),rate=occ_flow_rate))
    weather = Weather(initial_CO2)
    infiltration = Infiltration('inf1', inf_flow_rate)
    zone = Zone(volume, infiltration, weather)
    ventilation = Ventilation('vent1', ven_flow_rate)
    zone.add_inhabitants(*occs)
    zone.add_ventilations(ventilation)
    df_sim = zone.simulate(pd.date_range(start='01-january-2017',
                                         periods=1 * 24 * 6, freq='10MIN'),
                           co2_initial = initial_CO2)

    print(df_exact - df_sim)
    assert np.isclose(df_exact, df_sim).all()
