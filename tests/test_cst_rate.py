from __future__ import division, print_function # pour utiliser les fonctions de python 3
import minisim
import pandas as pd
import numpy as np
import pytest

@pytest.mark.skip(reason="no way of currently testing this")
def test_no_occup():
    vol = 100
    inf = minisim.Infiltration('inf', 10)
    w = minisim.Weather(450)
    
    vent = minisim.Ventilation('vent', 80)
    
    z = minisim.Zone(volume=vol, infiltration=inf, weather=w)
    
    z.add_ventilations(vent)
    daterange=pd.date_range(start='2017-01-01',end='2017-01-03', freq='1h')

    result_simu = z.simulate(daterange)
    r_diff = result_simu.diff()    
    
    assert (np.isclose(r_diff, 0, rtol=1e-06)).all()
    
@pytest.mark.skip(reason="no way of currently testing this")
def test_occup():
    vol = 100
    inf = minisim.Infiltration('inf',10)
    w = minisim.Weather(450)
    
    z = minisim.Zone(volume=vol, infiltration=inf, weather=w)
    
    vent = minisim.Ventilation('vent',80)
    z.add_ventilations(vent)
    
    r = 100
    
    occ = minisim.Inhabitant(name='sergei',rate=r)
    occ_neg = minisim.Inhabitant(name='nassiba', rate=-r)
    
    z.add_inhabitants(occ, occ_neg)

    daterange=pd.date_range(start='2017-01-01',end='2017-01-03', freq='1h')

    result_simu = z.simulate(daterange)
    r_diff = result_simu.diff()    
    
    assert (np.isclose(r_diff, 0, rtol=1e-06)).all()
