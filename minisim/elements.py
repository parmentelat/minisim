class Element(object):

    def __init__(self, name, rate):
        self.name = name
        self.rate = rate

    def __repr__(self):
        return '{} element with a rate of {} m3/s'.format(self.name, self.rate)


class Inhabitant(Element):
    def __init__(self, name, rate, concentration=36000):
        self.concentration = concentration
        Element.__init__(self, name, rate)


class Infiltration(Element):
    pass


class Ventilation(Element):
    pass


    
