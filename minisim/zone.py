# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

class Zone(object):
    """
    Un objet zone correspond à un volume clos éventuellement ventilé
    et avec une infiltration naturelle décrivant les défauts d'isolation
    """
    def __init__(self, volume, infiltration, weather):
        """
        Construit un objet zone vide avec une caractéristique d'infiltration
        La concentration extérieure en $CO_2$ 
        est obtenue au travers de l'objet meteo
        """
        self.volume = volume
        self.infiltration = infiltration
        self.weather = weather
        self.inhabitants = []
        self.ventilations = []

#    zone.add_inhabitants(
#        Inhabitant('in1', 12),
#        Inhabitant('in2', 24),


    def add_inhabitants(self, *inhabitants):
        self.inhabitants += list(inhabitants)

    def remove_inhabitants(self, *inhabitants):
        for inhabitant in inhabitants:
            self.inhabitants.remove(inhabitant)

    def add_ventilations(self, *ventilations):
        self.ventilations += list(ventilations)

    def remove_ventilations(self, *ventilations):
        for ventilation in ventilations:
            self.ventilations.remove(ventilation)

    def simulate(self, daterange, co2_initial = None):

        co2_initial = co2_initial or self.weather.co2_ext
        ext_total_flow_rate = self.infiltration.rate
        for ventil in self.ventilations :
            ext_total_flow_rate = ext_total_flow_rate + ventil.rate
        total_inhabitants_flow_rate = 0
        for inhabitant in self.inhabitants:
            total_inhabitants_flow_rate = total_inhabitants_flow_rate + inhabitant.rate
        concentration_co2 = np.array(np.zeros(len(daterange)))
        concentration_co2[0] = co2_initial
        dt = float(pd.Timedelta(daterange.freq).seconds)
        print (dt)
        for i in range(1, len(daterange)):
            concentration_co2[i] = (1 - (ext_total_flow_rate * dt)/(1.2 * self.volume))*concentration_co2[i-1]+\
                                   (total_inhabitants_flow_rate*dt*self.inhabitants[0].concentration)/self.volume + \
                                   ((ext_total_flow_rate * dt) / (1.2 * self.volume))* self.weather.co2_ext
        return pd.Series(concentration_co2, daterange)


if __name__ == '__main__':
    infi = Infiltration("infiltration", 5)
    weather = Weather(400)
    ma_zone = Zone(50, infi, weather)
    hab = Inhabitant('Bob', 36000)
    hab_bis = Inhabitant ('Marie', 36000)
    ma_zone.add_inhabitants(hab)
    daterange_test = pd.date_range('1/1/2017', periods = 100, freq = 'H')

    print (ma_zone.simulate(daterange_test))

    
